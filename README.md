# go-scp

A simple go implementation of the SCP/FBA


## Install

```
go get bitbucket.org/anarcher/go-scp/cmd/scp
```

```
$./scp
NAME:
   scp - scp: the simple scp(fba) client/server implementation

USAGE:
   scp [global options] command [command options] [arguments...]

VERSION:
   0.0.0

COMMANDS:
     server, s
     client, c
     help, h    Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

## Servers

This server and client use  Twirp[1] RPC library. There is the protobuf inteface file for it.(https://bitbucket.org/anarcher/go-scp/src/master/pkg/rpc/pb/server.proto)


```
$./scp s -h
NAME:
   scp server -

USAGE:
   scp server [command options] [arguments...]

OPTIONS:
   --port value, -p value        (default: 8000)
   --node_id value, --id value   (default: "v1")
   --threshold value, -t value   (default: 50)
   --validators value, -v value  {nodeID,IP}  -v=v1,localhost:8000 -v=v2,localhost:8001 ...
```

Example:
```
$scp s -id v1 -p 8000 -v v2,localhost:8001 -v v3,localhost:8002 
$scp s -id v2 -p 8001 -v v3,localhost:8002 -v v1,localhost:8000
$scp s -id v3 -p 8002 -v v2,localhost:8001 -v v1,localhost:8000
```


## Client

```
$./scp c n -h
NAME:
   scp client nominate -

USAGE:
   scp client nominate [command options] [arguments...]

OPTIONS:
   --node_id value, --id value   (default: "v1")
   --addr value, -a value        (default: "http://localhost:8000")
   --slot_index value, -s value  (default: 1)
   --value value, -v value

```

Example: 

```
$scp c n -s 1 -a http://localhost:8000 -node_id v1 -v hello

```



^[1]: https://github.com/twitchtv/twirp
