package client

import (
	"bitbucket.org/anarcher/go-scp/pkg/log"
	"bitbucket.org/anarcher/go-scp/pkg/rpc/pb"
	"bitbucket.org/anarcher/go-scp/pkg/scp"
	kitlog "github.com/go-kit/kit/log"

	"context"
	"fmt"
	"net/http"
	"strings"
)

type Client struct {
	twirpClient pb.SCP
	logger      kitlog.Logger
}

func NewClient(addr string) *Client {
	if !strings.HasPrefix(addr, "http://") {
		addr = fmt.Sprintf("http://%s", addr)
	}
	pbClient := pb.NewSCPProtobufClient(addr, &http.Client{})
	c := &Client{
		twirpClient: pbClient,
		logger:      log.With(log.Logger, "m", "Client"),
	}
	return c
}

func (c *Client) Nominate(ctx context.Context, m *scp.NominationMessage) error {

	nodeID := string(m.NodeID)
	n := &pb.Nomination{
		SlotIndex: uint64(m.Nomination.SlotIndex),
		Value:     m.Nomination.Value,
	}

	req := &pb.NominateRequest{
		NodeID:     nodeID,
		Nomination: n,
	}
	_, err := c.twirpClient.Nominate(ctx, req)
	if err != nil {
		log.Error(c.logger).Log("err", err)
	}
	return err
}

func (c *Client) Prepare(ctx context.Context, m *scp.BallotMessage) error {
	nodeID := string(m.NodeID)
	b := &pb.Ballot{
		SlotIndex: uint64(m.Ballot.SlotIndex),
		Value:     m.Ballot.Value,
	}

	req := &pb.PrepareRequest{
		NodeID: nodeID,
		Ballot: b,
	}
	_, err := c.twirpClient.Prepare(ctx, req)
	if err != nil {
		log.Error(c.logger).Log("err", err)
	}
	return err
}

func (c *Client) Accept(ctx context.Context, m *scp.BallotMessage) error {
	nodeID := string(m.NodeID)
	b := &pb.Ballot{
		SlotIndex: uint64(m.Ballot.SlotIndex),
		Value:     m.Ballot.Value,
	}
	req := &pb.AcceptRequest{
		NodeID: nodeID,
		Ballot: b,
	}
	_, err := c.twirpClient.Accept(ctx, req)
	if err != nil {
		log.Error(c.logger).Log("err", err)
	}
	return err
}
