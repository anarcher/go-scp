package server

import (
	"bitbucket.org/anarcher/go-scp/pkg/log"
	"bitbucket.org/anarcher/go-scp/pkg/rpc/pb"
	"bitbucket.org/anarcher/go-scp/pkg/scp"
	"github.com/davecgh/go-spew/spew"
	kitlog "github.com/go-kit/kit/log"

	"context"
)

type Server struct {
	service *scp.Service
	logger  kitlog.Logger
}

func NewServer(service *scp.Service) *Server {
	s := &Server{
		service: service,
		logger:  log.With(log.Logger, "m", "Server"),
	}
	return s
}

func (s *Server) Nominate(ctx context.Context, req *pb.NominateRequest) (*pb.NominateResponse, error) {
	l := log.With(s.logger, "rpc", "Nominate")
	log.Debug(l).Log("req", spew.Sprintf("%v", req))

	nodeID := scp.NodeID(req.NodeID)
	slotIndex := uint(req.Nomination.SlotIndex)
	value := req.Nomination.Value

	log.Debug(l).Log("nodeID", nodeID, "slotIndex", slotIndex, "value", spew.Sprintf("%s", value))
	msg := &scp.NominationMessage{
		NodeID: nodeID,
		Nomination: &scp.Nomination{
			SlotIndex: slotIndex,
			Value:     value,
		},
	}
	log.Debug(l).Log("msg", spew.Sprintf("%v", msg))

	err := s.service.Nominate(ctx, msg)
	if err != nil {
		log.Error(l).Log("err", err)
	}

	res := &pb.NominateResponse{}
	return res, err
}

func (s *Server) Prepare(ctx context.Context, req *pb.PrepareRequest) (*pb.PrepareResponse, error) {
	l := log.With(s.logger, "rpc", "Prepare")

	nodeID := scp.NodeID(req.NodeID)
	slotIndex := uint(req.Ballot.SlotIndex)
	value := req.Ballot.Value

	log.Debug(l).Log("nodeID", nodeID, "slotIndex", slotIndex, value, string(value))

	msg := &scp.BallotMessage{
		NodeID: nodeID,
		Ballot: &scp.Ballot{
			SlotIndex: slotIndex,
			Value:     value,
		},
	}

	err := s.service.Prepare(ctx, msg)
	if err != nil {
		log.Error(l).Log("err", err)
	}

	res := &pb.PrepareResponse{}
	return res, err
}

func (s *Server) Accept(ctx context.Context, req *pb.AcceptRequest) (*pb.AcceptResponse, error) {
	l := log.With(s.logger, "rpc", "Accept")
	log.Debug(l).Log("req", spew.Sprintf("%v", req))

	nodeID := scp.NodeID(req.NodeID)
	slotIndex := uint(req.Ballot.SlotIndex)
	value := req.Ballot.Value

	log.Debug(l).Log("nodeID", nodeID, "slotIndex", slotIndex, "value", string(value))

	msg := &scp.BallotMessage{
		NodeID: nodeID,
		Ballot: &scp.Ballot{
			SlotIndex: slotIndex,
			Value:     value,
		},
	}

	err := s.service.Accept(ctx, msg)
	if err != nil {
		log.Error(l).Log("err", err)
	}

	res := &pb.AcceptResponse{}
	return res, err
}
