package scp

import (
	"bytes"
)

type Ballot struct {
	SlotIndex uint
	Value     []byte
}

type BallotMessage struct {
	NodeID NodeID
	Ballot *Ballot
}

func NewBallot(slotIndex uint, value []byte) *Ballot {
	b := &Ballot{
		SlotIndex: slotIndex,
		Value:     value,
	}
	return b
}

func NewBallotMessage(nodeID NodeID, b *Ballot) *BallotMessage {
	m := &BallotMessage{
		NodeID: nodeID,
		Ballot: b,
	}
	return m
}

func (b *Ballot) EqValue(value []byte) bool {
	return bytes.Equal(b.Value, value)
}
