package scp

import (
	"bytes"
)

type Nomination struct {
	SlotIndex uint
	Value     []byte
}

type NominationMessage struct {
	NodeID     NodeID
	Nomination *Nomination
}

func (n Nomination) EqValue(value []byte) bool {
	return bytes.Equal(n.Value, value)
}
