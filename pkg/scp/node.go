package scp

type NodeID string

type Node interface {
	NodeID() NodeID
}

type LocalNode struct {
	id     NodeID
	Slot   map[uint]*Slot
	Quorum *Quorum
}

func NewLocalNode(id NodeID, quorum *Quorum) *LocalNode {
	n := &LocalNode{
		id:     id,
		Slot:   make(map[uint]*Slot),
		Quorum: quorum,
	}
	return n
}

func (n LocalNode) NodeID() NodeID {
	return n.id
}

func (n LocalNode) MinimumQuorumSize() int {
	return n.Quorum.MinimumSize()
}

type RemoteNode struct {
	id     NodeID
	Client Client
}

func NewRemoteNode(id NodeID, c Client) *RemoteNode {
	n := &RemoteNode{
		id:     id,
		Client: c,
	}
	return n
}

func (n RemoteNode) NodeID() NodeID {
	return n.id
}
