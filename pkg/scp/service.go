package scp

import (
	"bitbucket.org/anarcher/go-scp/pkg/log"
	kitlog "github.com/go-kit/kit/log"

	"context"
	"time"
)

type Service struct {
	slots  map[uint]*Slot
	node   *LocalNode
	stop   chan chan struct{}
	logger kitlog.Logger
}

func NewService(node *LocalNode) *Service {
	s := &Service{
		slots:  make(map[uint]*Slot),
		node:   node,
		stop:   make(chan chan struct{}),
		logger: log.With(log.Logger, "m", "Service"),
	}
	go s.loop()
	return s
}

func (s *Service) Nominate(ctx context.Context, msg *NominationMessage) error {
	infol := log.With(log.Info(s.logger), "f", "Nominate")

	n := msg.Nomination
	slot := s.Slot(n.SlotIndex)
	if err := slot.ValidateNomination(n); err != nil {
		return err
	}
	if !slot.AddNomination(n) {
		infol.Log("slot.AddNomination", false)
		return nil
	}

	// Send it to quorum members
	q := s.node.Quorum
	m := &NominationMessage{
		NodeID:     s.node.NodeID(),
		Nomination: n,
	}

	for _, v := range q.Validators {
		v.Client.Nominate(ctx, m)
	}
	infol.Log("from", s.node.NodeID(), "receive", "Nominate", "slot", n.SlotIndex, "value", string(n.Value))

	return nil
}

func (s *Service) Prepare(ctx context.Context, m *BallotMessage) error {
	infol := log.With(log.Info(s.logger), "f", "Prepare", "from", m.NodeID)

	slot := s.Slot(m.Ballot.SlotIndex)
	if !slot.AddVote(m.NodeID, m.Ballot) {
		infol.Log("slot.AddVote", false)
		return nil
	}

	infol.Log("receive", "Prepare", "slot", slot.Index, "value", string(m.Ballot.Value))
	return nil
}

func (s *Service) Accept(ctx context.Context, m *BallotMessage) error {
	infol := log.With(log.Info(s.logger), "f", "Accept")

	slot := s.Slot(m.Ballot.SlotIndex)
	if !slot.AddAccept(m.NodeID, m.Ballot) {
		infol.Log("slot.AddAccept", false)
		return nil
	}

	infol.Log("from", s.node.NodeID(), "receive", "Accept", "slot", slot.Index, "value", string(m.Ballot.Value))
	return nil
}

func (s *Service) Slot(idx uint) *Slot {
	slot, ok := s.slots[idx]
	if !ok {
		slot = NewSlot(idx, s.node)
		s.slots[idx] = slot
		return slot
	}
	return slot
}

func (s *Service) loop() {
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			for _, slot := range s.slots {
				slot.Externalize()
				slot.Accept(s.sendAccept)
				slot.Vote(s.sendPrepare)
			}
			//TODO: no ctx
		case <-s.stop:
			return
		}
	}
}

func (s *Service) Stop() {
	c := make(chan struct{})
	s.stop <- c
	<-c
}

func (s *Service) sendPrepare(m *BallotMessage) {
	ctx := context.Background()

	for _, n := range s.node.Quorum.Validators {
		log.Info(s.logger).Log("f", "sendPrepare", "to", n.NodeID(), "nodeID", m.NodeID, "value", string(m.Ballot.Value))
		n.Client.Prepare(ctx, m)
	}
}

func (s *Service) sendAccept(m *BallotMessage) {
	ctx := context.Background()

	for _, n := range s.node.Quorum.Validators {
		n.Client.Accept(ctx, m)
	}
}

func (s *Service) NodeID() NodeID {
	return s.node.NodeID()
}
