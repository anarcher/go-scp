package scp

import (
	"context"
)

type Client interface {
	Nominate(context.Context, *NominationMessage) error
	Prepare(context.Context, *BallotMessage) error
	Accept(context.Context, *BallotMessage) error
}
