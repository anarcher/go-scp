package scp

import (
	"testing"
)

func setupSimpleSlot() *Slot {
	remoteNodes := []*RemoteNode{
		&RemoteNode{
			id: NodeID("v1"),
		},
		&RemoteNode{
			id: NodeID("v2"),
		},
		&RemoteNode{
			id: NodeID("v3"),
		},
		&RemoteNode{
			id: NodeID("v4"),
		},
	}
	quorum := NewQuorum(remoteNodes, 50)
	node := NewLocalNode(NodeID("v1"), quorum)
	slot := NewSlot(1, node)
	return slot
}

func Test_Slot_NominationVote(t *testing.T) {
	slot := setupSimpleSlot()
	n := &Nomination{
		SlotIndex: slot.Index,
		Value:     []byte("hello"),
	}
	if ret := slot.AddNomination(n); ret == false {
		t.Errorf("slot.AddNomination want:%v have:%v", true, ret)
	}

	if l := len(slot.Nomination); l != 1 {
		t.Errorf("slot.Nomination want:%v have:%v", 1, l)
	}

	sendAll := func(m *BallotMessage) {
		if !m.Ballot.EqValue(n.Value) {
			t.Errorf("sendAll want:%v have:%v", true, false)
		}
	}
	slot.Vote(sendAll)
}

func Test_SlotAccept(t *testing.T) {
	slot := setupSimpleSlot()
	n := &Nomination{
		SlotIndex: slot.Index,
		Value:     []byte("hello"),
	}
	slot.AddNomination(n)
	b := &Ballot{
		SlotIndex: slot.Index,
		Value:     n.Value,
	}
	if !slot.AddVote(slot.node.NodeID(), b) {
		t.Errorf("slot.AddVote want:%v have:%v", true, false)
	}

	if l := len(slot.Votes); l != 1 {
		t.Errorf("slot.Votes want:%v have:%v", 1, l)
	}

	sendAll := func(m *BallotMessage) {}

	if ret := slot.Accept(sendAll); ret != false {
		t.Errorf("It's wrong that slot.Accept agree with remote nodes %v", ret)
	}

	//add another remote node's votes to threshold
	slot.AddVote(NodeID("v2"), b)
	slot.AddVote(NodeID("v3"), b)
	if r := slot.Accept(sendAll); r != true {
		t.Errorf("slot.Accept doesn't agree with nodes %v", r)
	}
}

func Test_Slot_Accept_Externalize(t *testing.T) {
	slot := setupSimpleSlot()
	n := &Nomination{
		SlotIndex: slot.Index,
		Value:     []byte("hello"),
	}
	slot.AddNomination(n)

	b := &Ballot{
		SlotIndex: slot.Index,
		Value:     n.Value,
	}
	slot.AddAccept(slot.node.NodeID(), b)
	slot.AddAccept(NodeID("v1"), b)
	slot.AddAccept(NodeID("v2"), b)
	if r := slot.Externalize(); r != true {
		t.Errorf("Externalize want:%v have:%v", true, r)
	}
}
