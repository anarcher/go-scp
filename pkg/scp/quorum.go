package scp

import (
	"math"
)

type Quorum struct {
	Validators []*RemoteNode
	Threshold  float64
}

func NewQuorum(valiators []*RemoteNode, threshold float64) *Quorum {
	q := &Quorum{
		Validators: valiators,
		Threshold:  threshold,
	}
	return q
}

func (q Quorum) MinimumSize() int {
	s := float64(len(q.Validators)+1) * (q.Threshold / 100.0)
	return int(math.Floor(s))
}
