package scp

import (
	"testing"
)

func Test_QuorumMinimumSize(t *testing.T) {
	q := &Quorum{
		Validators: []*RemoteNode{
			&RemoteNode{
				id: NodeID("v1"),
			},
			&RemoteNode{
				id: NodeID("v2"),
			},
			&RemoteNode{
				id: NodeID("v3"),
			},
			&RemoteNode{
				id: NodeID("v4"),
			},
		},
		Threshold: 50,
	}
	size := q.MinimumSize()
	if size != 2 {
		t.Errorf("QuorumMinimumSize want:%v have:%v", 2, size)
	}

}
