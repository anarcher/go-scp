package scp

import (
	"bitbucket.org/anarcher/go-scp/pkg/log"
	kitlog "github.com/go-kit/kit/log"

	"bytes"
	"fmt"
)

type Slot struct {
	Index         uint
	Value         []byte
	Nomination    []*Nomination
	Votes         map[NodeID]*Ballot
	Confirmations map[NodeID]*Ballot
	node          *LocalNode
	logger        kitlog.Logger
}

func NewSlot(index uint, node *LocalNode) *Slot {

	s := &Slot{
		Index:         index,
		Nomination:    make([]*Nomination, 0),
		Votes:         make(map[NodeID]*Ballot),
		Confirmations: make(map[NodeID]*Ballot),
		node:          node,
		logger:        log.With(log.Logger, "m", "Slot"),
	}

	return s
}

func (s *Slot) AddNomination(n *Nomination) bool {
	for _, in := range s.Nomination {
		if bytes.Equal(in.Value, n.Value) {
			return false
		}
	}
	s.Nomination = append(s.Nomination, n)
	log.Info(s.logger).Log("nomination", len(s.Nomination))
	return true
}

func (s *Slot) Vote(sendAll func(*BallotMessage)) {
	if len(s.Nomination) <= 0 {
		log.Debug(s.logger).Log("nomination", len(s.Nomination))
		return
	}
	if _, ok := s.Votes[s.node.NodeID()]; ok {
		// don't vote if already vote
		return
	}

	n := s.Nomination[len(s.Nomination)-1]
	v := NewBallot(s.Index, n.Value)
	m := NewBallotMessage(s.node.NodeID(), v)
	s.AddVote(s.node.NodeID(), v)
	sendAll(m)
	log.Info(s.logger).Log("from", s.node.NodeID(), "sent", "vote", "slotIndex", s.Index, "value", string(n.Value))
}

func (s *Slot) AddVote(nodeID NodeID, b *Ballot) bool {
	if !s.existNomination(b.Value) {
		return false
	}

	for n, v := range s.Votes {
		s.logger.Log("nodeID", nodeID, "value", string(v.Value))

		if n == nodeID && !v.EqValue(b.Value) {
			return false
		}
	}

	s.Votes[nodeID] = b
	log.Info(s.logger).Log("f", "AddVote", "nodeID", nodeID, "value", string(b.Value))
	return true
}

func (s *Slot) AddAccept(nodeID NodeID, b *Ballot) bool {
	if !s.existNomination(b.Value) {
		log.Info(s.logger).Log("existNomination", false, "nodeID", nodeID, "value", string(b.Value))
		return false
	}

	for n, v := range s.Confirmations {
		if n == nodeID && !v.EqValue(b.Value) {
			return false
		}
	}

	s.Confirmations[nodeID] = b
	log.Info(s.logger).Log("f", "AddAccept", "nodeID", nodeID, "value", string(b.Value))
	return true
}

func (s *Slot) Accept(sendAll func(*BallotMessage)) bool {
	if _, ok := s.Confirmations[s.node.NodeID()]; ok {
		return false
	}
	log.Debug(s.logger).Log("votes", len(s.Votes))

	v, ok := s.Votes[s.node.NodeID()]
	if ok {
		if s.canAccept(v) {
			m := NewBallotMessage(s.node.NodeID(), v)
			s.Confirmations[s.node.NodeID()] = v
			sendAll(m)
			log.Info(s.logger).Log("status", "Accepted", "node", s.node.NodeID(), "slotIndex", s.Index, "value", string(v.Value))
			return true
		}
	}
	return false
}

func (s *Slot) Externalize() bool {
	v, ok := s.Confirmations[s.node.NodeID()]
	if !ok {
		return false
	}

	if s.canExternalize(v) {
		s.Nomination = s.Nomination[:0]
		s.Votes = make(map[NodeID]*Ballot)
		s.Confirmations = make(map[NodeID]*Ballot)
		//Externalize
		s.Value = v.Value
		log.Info(s.logger).Log("status", "Externalized", "node", s.node.NodeID(), "slotIndex", s.Index, "value", string(v.Value))
		return true
	}

	return false
}

func (s *Slot) ValidateNomination(n *Nomination) error {
	if len(s.Value) > 0 {
		return fmt.Errorf("this slot is Externalized")
	}
	return nil
}

func (s *Slot) existNomination(value []byte) bool {
	for _, n := range s.Nomination {
		if n.EqValue(value) == true {
			return true
		}
	}
	return false
}

func (s *Slot) canAccept(b *Ballot) bool {
	votes := 0
	for _, v := range s.node.Quorum.Validators {
		if _, ok := s.Votes[v.NodeID()]; ok {
			votes = votes + 1
		}
	}
	log.Debug(s.logger).Log("slotIndex", s.Index, "votes", votes, "size", s.node.Quorum.MinimumSize())

	if votes >= s.node.Quorum.MinimumSize() {
		//reaches agreement
		return true

	}
	return false
}

func (s *Slot) canExternalize(b *Ballot) bool {
	votes := 0
	for _, v := range s.node.Quorum.Validators {
		if _, ok := s.Confirmations[v.NodeID()]; ok {
			votes = votes + 1
		}
	}
	if votes >= s.node.Quorum.MinimumSize() {
		return true
	}
	return false
}
