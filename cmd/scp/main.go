package main

import (
	"bitbucket.org/anarcher/go-scp/pkg/log"
	"bitbucket.org/anarcher/go-scp/pkg/rpc/client"
	"bitbucket.org/anarcher/go-scp/pkg/rpc/pb"
	"bitbucket.org/anarcher/go-scp/pkg/rpc/server"
	"bitbucket.org/anarcher/go-scp/pkg/scp"
	"github.com/urfave/cli"

	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"strings"
)

func main() {

	app := cli.NewApp()
	app.Name = "scp"
	app.Usage = "scp: the simple scp(fba) client/server implementation"

	app.Commands = []cli.Command{
		{
			Name:      "server",
			ShortName: "s",
			Action:    serverAction,
			Flags: []cli.Flag{
				cli.IntFlag{
					Name:  "port,p",
					Value: 8000,
				},
				cli.StringFlag{
					Name:  "node_id,id",
					Value: "v1",
				},
				cli.IntFlag{
					Name:  "threshold,t",
					Value: 50,
				},
				cli.StringSliceFlag{
					Name:  "validators,v",
					Usage: "{nodeID,IP}  -v=v1,localhost:8000 -v=v2,localhost:8001 ...",
				},
			},
		},
		{
			Name:      "client",
			ShortName: "c",
			Subcommands: []cli.Command{
				{
					Name:      "nominate",
					ShortName: "n",
					Action:    clientNominateAction,
					Flags: []cli.Flag{
						cli.StringFlag{
							Name:  "node_id,id",
							Value: "v1",
						},
						cli.StringFlag{
							Name:  "addr,a",
							Value: "http://localhost:8000",
						},
						cli.IntFlag{
							Name:  "slot_index,s",
							Value: 1,
						},
						cli.StringFlag{
							Name:  "value,v",
							Value: "",
						},
					},
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Logger.Log("err", err)
		os.Exit(1)
	}
}

func serverAction(c *cli.Context) error {
	logger := log.With(log.Logger)
	log.Info(logger).Log("server", "starting")

	addr := fmt.Sprintf("0.0.0.0:%d", c.Int("port"))
	listener, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}

	threshold := float64(c.Int("threshold"))

	var validators []*scp.RemoteNode
	for _, v := range c.StringSlice("validators") {
		vv := strings.Split(v, ",")
		if len(vv) < 2 {
			return fmt.Errorf("validators is wrong")
		}
		client := client.NewClient(vv[1])
		nodeID := scp.NodeID(vv[0])
		node := scp.NewRemoteNode(nodeID, client)
		validators = append(validators, node)
		log.Info(logger).Log("validator", vv[0], "addr", vv[1])
	}

	log.Info(logger).Log("addr", addr, "threshold", threshold, "validators", len(validators))

	nodeID := scp.NodeID([]byte(c.String("node_id")))
	quorum := scp.NewQuorum(validators, threshold)

	node := scp.NewLocalNode(nodeID, quorum)
	svc := scp.NewService(node)
	server := server.NewServer(svc)
	twirpHandler := pb.NewSCPServer(server, nil)

	return http.Serve(listener, twirpHandler)
}

func clientNominateAction(c *cli.Context) error {
	ctx := context.Background()
	addr := c.String("addr")
	client := pb.NewSCPProtobufClient(addr, &http.Client{})

	nodeID := c.String("node_id")
	slotIndex := uint64(c.Int("slot_index"))
	value := []byte(c.String("value"))
	if len(value) <= 0 {
		fmt.Printf("value is empty")
		os.Exit(1)

	}
	n := &pb.Nomination{
		SlotIndex: slotIndex,
		Value:     value,
	}

	req := &pb.NominateRequest{
		NodeID:     nodeID,
		Nomination: n,
	}

	_, err := client.Nominate(ctx, req)
	if err != nil {
		fmt.Printf("err: %v\n", err)
		os.Exit(1)
	}

	return nil
}
